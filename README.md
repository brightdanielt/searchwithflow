What's this?
===============
A use case searching movies on TMDB with coroutine flow.<br>

Why I do this?
===============
After study some coroutine flow in medium and official website, I want to give it a try 😄

Something else
===============
Two years ago, I just know Rx and I didn't understand how to use those operators, now, the flow
brings me these operators again and I know that's all about concurrency.<br>
Thanks Google provide us tutorials about coroutines and flows in detail.

Reference
===============
[TMDB api - search movies]<br>
[Medium - Migrating from LiveData to Kotlin’s Flow]<br>
[Medium - Kotlin Flow API]<br>
[Medium - Exploring LiveData and Kotlin Flow]<br>
[Code lab - Learn advanced coroutines with Kotlin Flow and LiveData]<br>
[Diagrams - flow operators]<br>


[TMDB api - search movies]:https://developers.themoviedb.org/3/search/search-movies

[Medium - Migrating from LiveData to Kotlin’s Flow]:https://medium.com/androiddevelopers/migrating-from-livedata-to-kotlins-flow-379292f419fb

[Medium - Kotlin Flow API]:https://abhiappmobiledeveloper.medium.com/kotlin-flow-api-54994c60a9ae

[Medium - Exploring LiveData and Kotlin Flow]:https://medium.com/android-dev-hacks/exploring-livedata-and-kotlin-flow-7c8d8e706324

[Code lab - Learn advanced coroutines with Kotlin Flow and LiveData]:https://developer.android.com/codelabs/advanced-kotlin-coroutines

[Diagrams - flow operators]:https://flowmarbles.com/#conflate