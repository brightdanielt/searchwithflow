package com.cauliflower.searchwithflow

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

object NetworkService {

    val instance: TmdbService by lazy {
        val client = OkHttpClient.Builder().apply {
            addInterceptor(Interceptor())
            if (BuildConfig.DEBUG) {
                addInterceptor(
                    //Logs request and response lines and their respective headers and bodies (if present)
                    HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
                )
            }
        }.build()

        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://api.themoviedb.org/3/")
            .client(client)
            .build()
            .create(TmdbService::class.java)
    }
}

interface TmdbService {
    @GET("search/movie?language=en-US&page=1")
    suspend fun searchMovies(@Query("query") query: String): Response<MovieData>
}
