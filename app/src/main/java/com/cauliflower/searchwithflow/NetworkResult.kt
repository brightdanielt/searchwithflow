package com.cauliflower.searchwithflow

sealed class NetworkResult<out T> {

    object Loading : NetworkResult<Nothing>()

    data class Success<out T>(val data: T) : NetworkResult<T>()

    data class Error(val exception: Throwable) : NetworkResult<Nothing>()
}
