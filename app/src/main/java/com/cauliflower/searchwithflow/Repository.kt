package com.cauliflower.searchwithflow

import java.lang.Exception

class Repository private constructor(
    private val service: TmdbService
) {
    companion object {
        @Volatile
        private var instance: Repository? = null

        fun getInstance(service: TmdbService): Repository =
            instance ?: synchronized(this) {
                instance ?: Repository(service).also { instance = it }
            }
    }

    suspend fun searchMovie(query: String): NetworkResult<MovieData> {
        val response = service.searchMovies(query)
        return if (response.isSuccessful) {
            NetworkResult.Success(response.body()!!)
        } else {
            NetworkResult.Error(Exception(response.errorBody().toString()))
        }
    }

}