package com.cauliflower.searchwithflow

import android.app.Application

class MyApplication : Application() {

    fun getRepository(): Repository =
        Repository.getInstance(NetworkService.instance)
}