package com.cauliflower.searchwithflow

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class MovieAdapter : RecyclerView.Adapter<MovieAdapter.ViewHolder>() {

    private val movieList: ArrayList<Result> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_movie, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(movieList[position])
    }

    override fun getItemCount(): Int {
        return movieList.size
    }

    fun swapData(data: List<Result>) {
        movieList.clear()
        movieList.addAll(data)
        notifyDataSetChanged()
    }

    open class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(movie: Result) {
            val imgPoster = itemView.findViewById<ImageView>(R.id.img_poster)
            val tvTitle = itemView.findViewById<TextView>(R.id.tv_title)
            Picasso.get().load("https://image.tmdb.org/t/p/w200" + movie.poster_path)
                .placeholder(R.drawable.ic_baseline_image_24)
                .into(imgPoster)
            tvTitle.text = movie.title
        }
    }
}