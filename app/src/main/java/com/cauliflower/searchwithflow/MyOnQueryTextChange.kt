package com.cauliflower.searchwithflow

import androidx.appcompat.widget.SearchView

class MyOnQueryTextChange(
    private val submitBlock: (query: String?) -> Boolean = { false },
    private val changeBlock: (query: String?) -> Boolean = { false }
) : SearchView.OnQueryTextListener {
    override fun onQueryTextSubmit(query: String?): Boolean {
        return submitBlock.invoke(query)
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return changeBlock.invoke(newText)
    }
}