package com.cauliflower.searchwithflow

import androidx.lifecycle.*

class SearchViewModel(private val repo: Repository) : ViewModel() {

    private val query = MutableLiveData<String>()
    private val _result: LiveData<NetworkResult<MovieData>> = query.switchMap {
        liveData {
            emit(NetworkResult.Loading)
            emit(searchMovie(it))
        }
    }

    val result: LiveData<NetworkResult<MovieData>> = _result

    fun updateQuery(query: String?) {
        if (query?.isNotEmpty() == true && this.query.value != query) {
            this.query.value = query
        }
    }

    private suspend fun searchMovie(query: String): NetworkResult<MovieData> {
        return repo.searchMovie(query)
    }

    class Factory(private val repo: Repository) : ViewModelProvider.Factory {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(modelClass: Class<T>): T = SearchViewModel(repo) as T
    }
}