package com.cauliflower.searchwithflow

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {

    private val viewmodel: SearchViewModel by viewModels {
        SearchViewModel.Factory(
            (application as MyApplication).getRepository()
        )
    }

    private val adapter: MovieAdapter by lazy { MovieAdapter() }
    private val loading by lazy { findViewById<ProgressBar>(R.id.pgBar) }
    private val rvMovie by lazy { findViewById<RecyclerView>(R.id.rv) }
    private val searchView by lazy { findViewById<SearchView>(R.id.searchView) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rvMovie.adapter = adapter
        searchView.setOnQueryTextListener(MyOnQueryTextChange { query ->
            viewmodel.updateQuery(query)
            return@MyOnQueryTextChange false
        })

        viewmodel.updateQuery("鷹眼")
        viewmodel.result.observe(this, Observer {
            when (it) {
                is NetworkResult.Success -> {
                    rvMovie.visibility = View.VISIBLE
                    loading.visibility = View.INVISIBLE
                    adapter.swapData(it.data.results)
                }
                else -> {
                    rvMovie.visibility = View.INVISIBLE
                    loading.visibility = View.VISIBLE
                }
            }
        })
    }
}